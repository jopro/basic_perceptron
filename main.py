from case import *
from sigmoid_inceptron import *
from perceptron import *
from inceptron import *

def main():
	cases = {"OR":
			[Case([0,0], False),
			Case([0,1], True),
			Case([0.4,0], False),
			Case([0,0.4], False),
			Case([0.4,0.4], False),
			Case([0.6,0], True),
			Case([0,0.6], True),
			Case([1,0], True),
			Case([1,1], True)],
		"AND":
			[Case([0,0], False),
			Case([0,1], False),
			Case([1,0], False),
			Case([1,1], True)],
		"NAND":
			[Case([0,0], True),
			Case([0,1], True),
			Case([1,0], True),
			Case([1,1], False)],
		"NOR":
			[Case([0,0], True),
			Case([0,1], False),
			Case([0.4,0], True),
			Case([0,0.4], True),
			Case([0.4,0.4], True),
			Case([0.6,0], False),
			Case([0,0.6], False),
			Case([1,0], False),
			Case([1,1], False)],
		"XOR":
			[Case([0,0], False),
			Case([0,1], True),
			Case([1,0], True),
			Case([1,1], False)],
		"NOT": 
			[Case([0,0], True),
			Case([0,1], True),
			Case([0,0.5], True),
			Case([1,0], False),
			Case([1,1], False)],
		"ODD":
			[Case([0,0,0,0,0], False),
			Case([0,0,0,0,1], True),
			Case([0,0,0,1,0], False),
			Case([0,0,0,1,1], True),
			Case([0,0,1,0,0], False),
			Case([0,0,1,0,1], True),
			Case([0,0,1,1,0], False),
			Case([0,0,1,1,1], True),
			Case([0,1,0,0,0], False),
			Case([0,1,0,0,1], True),
			Case([0,1,0,1,0], False),
			Case([0,1,0,1,1], True),
			Case([0,1,1,0,0], False),
			Case([0,1,1,0,1], True),
			Case([0,1,1,1,0], False),
			Case([0,1,1,1,1], True)],
		"N%3":
			[Case([0,0,0,0,0], False),
			Case([0,0,0,0,1], False),
			Case([0,0,0,1,0], False),
			Case([0,0,0,1,1], True),
			Case([0,0,1,0,0], False),
			Case([0,0,1,0,1], False),
			Case([0,0,1,1,0], True),
			Case([0,0,1,1,1], False),
			Case([0,1,0,0,1], True)]
	}


	test = "something"
	while test != "":
		while test != "" and test not in cases.keys():
			test = raw_input("Do test? Options:"+str(cases.keys()))
		if test != "":
			print(test)
			for method in (Inceptron([], 0.1), Perceptron([], 0.1)):#, Sigceptron([], 0.1)):
				tester = method
				tester.learnInputs(cases[test])
			test = "something"		

if __name__ == '__main__':
	main()
