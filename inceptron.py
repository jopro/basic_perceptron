from case import *

class Inceptron():
	def __init__(self, weights=[], learningRate=1):
		self.weights = weights
		self.learningRate = learningRate

	def classify(self, inputs):
		while(len(inputs) > len(self.weights)-1):
			self.weights.append(0)#should be a random value
		total = self.weights[-1]#add the bias
		for index in range(len(inputs)):
			total += self.weights[index]*inputs[index]

		return total>0

	def trainValue(self, inputs, case):
		returnedCase = self.classify(inputs)

		if returnedCase == case:
			return True#was correct
		
		update = 1
		if returnedCase and not case:
			update = -1
		
		inputs = list(inputs)
		inputs.append(1)#add bias

		for index in range(len(inputs)):
			w = self.weights[index]
			w += self.learningRate*update*inputs[index]
			w = max(min(w, 1), -1)
			self.weights[index] = w

		return False#had to update

	def learnInputs(self, cases):
		correct = False
		iters = 0
		xored = False
		while not correct:	
			correct = True
			for case in cases:
				if not xored:
					wasCorrect = self.trainValue(case.realInputs, case.case)
				else:
					wasCorred = self.trainValue(case.inputs, case.case)
				if not wasCorrect:
					correct = False
			iters += 1
			if iters > 10000:
				if xored:
					print("May not be lineary separable")
					break
				else:
					iters = 0
					xored = True

		print("Iters:"+str(iters))
		print("Weights:"+str(self.weights))
	
		for case in cases:
			print("Case:"+str(case))

		if len(case.realInputs) == 2:
			
			for y in range(11, -2, -1):
				out = ""
				for x in range(-1, 11, 1):
					char = "-"
					case = Case([x/10.0, y/10.0], None)
					if self.classify(case.inputs):
						char = "+"
					out += char
				print(out)
		else:
			for i in range(1, 32):
				bits = map(int, list(bin(i)[2:]))
				
				bits.reverse()
				while len(bits) < 5:
					bits.append(0)
				bits.reverse()
				case = Case(bits, None)
				print(str(i)+": "+str(self.classify(case.inputs)))
