from case import *

class Perceptron():

	def __init__(self, weights=[], learningRate=1):
		self.weights = weights
		self.learningRate = learningRate

	def classify(self, inputs):
		while(len(inputs) > len(self.weights)-1):
			self.weights.append(0)#should be a random value
		total = self.weights[-1]#add the bias
		for index in range(len(inputs)):
			total += self.weights[index]*inputs[index]

		return total>0

	def trainValue(self, inputs, case):
		returnedCase = self.classify(inputs)

		if returnedCase == case:
			return True#was correct
		
		update = 1
		if returnedCase and not case:
			update = -1
		
		inputs = list(inputs)
		inputs.append(1)

		for index in range(len(inputs)):
			w = self.weights[index]
			w += self.learningRate*update*inputs[index]
			w = max(min(w, 1), -1)
			
			self.weights[index] = w

		return False#had to update

	def learnInputs(self, cases):
		correct = False
		iters = 0
		while not correct:	
			correct = True
			for case in cases:
				wasCorrect = self.trainValue(case.inputs, case.case)
				if not wasCorrect:
					correct = False
			iters += 1
			if iters > 1000000:
				print("May not be lineary separable")
				break

		print("Iters:"+str(iters))
		print("Weights:"+str(self.weights))
	
		for case in cases:
			print("Case:"+str(case.inputs)+" : "+str(self.classify(case.inputs)))

		for y in range(11, -1, -1):
			out = ""
			for x in range(0, 11, 1):
				char = "-"
				if self.classify([x/10.0, y/10.0]):
					char = "+"
				out += char
			print(out)
