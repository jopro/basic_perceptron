class Case:
	def __init__(self, inputs, case):
		self.realInputs = inputs
		self.inputs = list(inputs)
		for i in range(len(inputs)):
			for j in range(i+1, len(inputs)):
				a = inputs[i]
				b = inputs[j]

				xor = abs(a-b)
				self.inputs.append(xor)

		self.case = case
	def __str__(self):
		o = ""
		o += str(self.realInputs)
		if self.case != None:
			o+= ": "+str(self.case)
		return o
